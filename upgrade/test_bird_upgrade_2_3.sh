#!/bin/bash

set -e

# update system first
apt update
apt upgrade -y

# prepare vers/ dir
mkdir -p vers

# install default distro bird2
apt install -y bird2
./upgrade/bird_versions.sh | tee vers/distro

# enable bird2 CZ.NIC Labs repos
apt-get install -y wget
wget https://pkg.labs.nic.cz/doc/scripts/enable-repo-cznic-labs.sh
bash enable-repo-cznic-labs.sh bird2

# upgrade to latest v2
apt-get install -y bird2
./upgrade/bird_versions.sh | tee vers/v2

# run packaging tests for v2
git clone https://gitlab.nic.cz/labs/bird.git
pushd bird
apkg test --test-dep
popd

# enable bird3 CZ.NIC Labs repos
bash enable-repo-cznic-labs.sh bird3

# upgrade to latest v3
apt-get install -y bird3
./upgrade/bird_versions.sh | tee vers/v3

# run packaging tests for v3
pushd bird
git checkout thread-next
apkg test --test-dep
popd

# print versions summary
print_vers () {
    echo
    echo "# $1 versions:"
    cat vers/$1
}
print_vers distro
print_vers v2
print_vers v3
