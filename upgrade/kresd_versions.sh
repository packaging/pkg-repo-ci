#!/bin/bash

source "$(dirname "$0")/../common.sh"

kresd --version
dpkg -l 'knot-resolver*' | pretty_dpkgl
dpkg -l 'libknot*' | pretty_dpkgl
