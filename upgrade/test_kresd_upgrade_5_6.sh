#!/bin/bash

set -e

# update system first
apt update
apt upgrade -y

# prepare vers/ dir
mkdir -p vers

# install default distro knot-resolver (v5)
apt install -y knot-resolver
./upgrade/kresd_versions.sh | tee vers/distro

# enable CZ.NIC Labs repos
apt-get install -y wget
wget https://pkg.labs.nic.cz/doc/scripts/enable-repo-cznic-labs.sh
bash enable-repo-cznic-labs.sh knot-resolver

# upgrade to latest v5
apt-get install -y knot-resolver
./upgrade/kresd_versions.sh | tee vers/v5

# no distro/tests in v5

# upgrade to latest v6
apt-get install -y knot-resolver6
./upgrade/kresd_versions.sh | tee vers/v6

# run packaging tests
git clone https://gitlab.nic.cz/knot/knot-resolver
pushd knot-resolver
../checkout_latest_tag.sh
apkg test --test-dep
popd

# print versions summary
print_vers () {
    echo
    echo "# $1 versions:"
    cat vers/$1
}
print_vers distro
print_vers v5
print_vers v6
