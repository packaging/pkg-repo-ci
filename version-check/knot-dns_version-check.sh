#!/bin/bash
# version checks for knot package

set -e

source "$(dirname "$0")/../common.sh"

LATEST_VERSION=$($(dirname "$0")/knot-dns_upstream-version.sh)
BIN_VERSION=$(knotd --version 2>&1 | only_version)
FULL_VERSION=$(dpkg -l knot | pretty_dpkgl '\2')

source "$(dirname "$0")/version-check-common.sh"
