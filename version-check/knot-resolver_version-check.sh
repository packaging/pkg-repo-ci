#!/bin/bash
# version checks for knot-resolver6 package

set -e

source "$(dirname "$0")/../common.sh"

LATEST_VERSION=$($(dirname "$0")/knot-resolver_upstream-version.sh)
BIN_VERSION=$(kresd --version 2>&1 | only_version)
FULL_VERSION=$(dpkg -l knot-resolver6 | pretty_dpkgl '\2')

source "$(dirname "$0")/version-check-common.sh"
