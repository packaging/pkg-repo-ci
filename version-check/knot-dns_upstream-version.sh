#!/bin/bash
# return latest upstream version of Knot DNS
set -o errexit

REPO=https://gitlab.nic.cz/knot/knot-dns.git

git ls-remote --tags --refs $REPO | cut -f2- | sed -n "s#^refs/tags/v##p" | grep -v dev | sort -V | tail -1
