# This shared code expects:
#
# BIN_VERSION=1.2.3
# LATEST_VERSION=1.2.3
# FULL_VERSION=1.2.3-cznic.1
#
# Fill those in and source this.
#
# To print checks but don't fail on error:
#
# ON_ERROR=warn

PKG_VERSION=$(echo "$FULL_VERSION" | cut -d '-' -f 1)
PKG_RELEASE=$(echo "$FULL_VERSION" | cut -d '-' -f 2)

echo -e "# VERSION CHECK\n"

echo "Latest version:  $LATEST_VERSION"
echo "Binary version:  $BIN_VERSION"

echo "Package version: $PKG_VERSION"
echo "Package release: $PKG_RELEASE"
echo "Full version:    $FULL_VERSION"

echo
VCHECK_FAIL=0

if [[ "$PKG_VERSION" == "$BIN_VERSION" ]]; then
    echo "✔ package version $PKG_VERSION matches binary version"
else
    echo "ERROR: package version $PKG_VERSION doesn't match binary version $BIN_VERSION :("
    VCHECK_FAIL=10
fi

if [[ "$BIN_VERSION" == "$LATEST_VERSION" ]]; then
    echo "✔ latest version $LATEST_VERSION is installed"
else
    echo "ERROR: latest version is $LATEST_VERSION, but $BIN_VERSION is installed :("
    VCHECK_FAIL=11
fi

if echo "$PKG_RELEASE" | grep -q 'cznic'; then
    echo "✔ upstream CZ.NIC package release $PKG_RELEASE installed"
else
    echo "ERROR: distribution package version $FULL_VERSION installed instead of upstream package :("
    apt-cache show bird2
    VCHECK_FAIL=12
fi

echo
if [[ "$VCHECK_FAIL" -eq 0 ]]; then
    echo "✔ all version checks succeeded, yay \\o/"
else
    if [[ "$ON_ERROR" == "warn" ]]; then
        echo "WARNING: some version checks failed, see above ^"
    else
        echo "ERROR: some version checks failed, see above ^"
        exit $VCHECK_FAIL
    fi
fi
