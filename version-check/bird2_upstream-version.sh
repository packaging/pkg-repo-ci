#!/bin/bash
# return latest upstream version of BIRD 2.x
set -o errexit

REPO=https://gitlab.nic.cz/labs/bird.git

git ls-remote --tags --refs $REPO | cut -f2- | sed -n "s#^refs/tags/v##p" | grep '^2\.' | sort -V | tail -1
