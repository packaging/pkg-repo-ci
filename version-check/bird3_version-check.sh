#!/bin/bash
# version checks for bird3 package

set -e

source "$(dirname "$0")/../common.sh"

LATEST_VERSION=$($(dirname "$0")/bird3_upstream-version.sh)
BIN_VERSION=$(bird --version 2>&1 | only_version)
FULL_VERSION=$(dpkg -l bird3 | pretty_dpkgl '\2')

source "$(dirname "$0")/version-check-common.sh"
