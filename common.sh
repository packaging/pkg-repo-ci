# shared shell code for pkg-repo-ci

# strip `$BINARY --version` output to only version
only_version () {
    cat | sed -r 's#.* ([0-9]+\.[0-9][^ ]+)#\1#'
}

# pretty print dpkg -l output
pretty_dpkgl () {
    fmt=$1
    if [[ -z "$fmt" ]]; then
        fmt='\1 \2'
    fi
    # parse and filter `dpkg -l` output
    grep '^ii' | sed -r "s/ii +([^ ]+) +([^ ]+) +.*/$fmt/"
}
