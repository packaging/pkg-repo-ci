#!/bin/bash

set -e

LATEST_TAG=$(git describe --tags --abbrev=0)
echo "Checking out latest version tag: $LATEST_TAG"

set -x
git checkout "$LATEST_TAG"
